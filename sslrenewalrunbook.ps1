#######################################################################################
# Script that renews a Let's Encrypt certificate for an Azure Application Gateway
# Pre-requirements:
#      - Have a storage account in which the folder path has been created: 
#        '/.well-known/acme-challenge/', to put here the Let's Encrypt DNS check files
#
#      - Add "Path-based" rule in the Application Gateway with this configuration: 
#           - Path: '/.well-known/acme-challenge/*'
#           - Backend: <Blob public path of the previously created storage account>
#                - Example: 'test.blob.core.windows.net'
#           - BackendConf
#                - Path": "/public/.well-known/acme-challenge/",
#
#      - For execution on Azure Automation, import:
#        ACMESharp, Az.Accounts, Az.KeyVault, Az.Network, Az.Storage
#      - It also expects "$domains" in JSON array format
#           - ["www.example.com","site.example.com"]
#######################################################################################

[CmdletBinding()]
Param(
    [Parameter(Mandatory)]
    [string[]]$domains,

    [Parameter(Mandatory)]
    [string]$emailAddress,

    [Parameter(Mandatory)]
    [string]$resourceGroup,

    [Parameter(Mandatory)]
    [string]$storageName,

    [Parameter(Mandatory)]
    [string]$keyVault,

    [Parameter(Mandatory)]
    [string]$certificatePassword
)

$securePassword = ConvertTo-SecureString -String $certificatePassword -AsPlainText -Force

## Azure Login ##

# If Runbook for Azure Automation
$connection = Get-AutomationConnection -Name AzureRunAsConnection
Connect-AzAccount -ServicePrincipal -Tenant $connection.TenantID -ApplicationID $connection.ApplicationID -CertificateThumbprint $connection.CertificateThumbprint

# DEBUG Initialize-ACMEVault -BaseService LetsEncrypt-STAGING
Initialize-ACMEVault
New-ACMERegistration -Contacts mailto:$emailAddress -AcceptTos

$aliases = @()
$blobs = @()
$tmpPath = $env:TEMP + "\"
$storageAccount = Get-AzStorageAccount -ResourceGroupName $resourceGroup -Name $storageName
$ctx = $storageAccount.Context

foreach ($domain in $domains)
{
    # Set the alias for the certificate. E.g., instead of example.com it would be example-com
    $AliasDns = $domain.replace(".", "-")

    $aliases += $AliasDns

    New-ACMEIdentifier -Dns $domain -Alias $AliasDns
    (Complete-ACMEChallenge -IdentifierRef $AliasDns -ChallengeType http-01 -Handler manual).Challenge
    $http01 = (Update-ACMEIdentifier -IdentifierRef $AliasDns -ChallengeType http-01).Challenges | Where-Object { $_.Type -eq "http-01" }

    # Add file blob to check DNS
    $FileContentStrIndex = $http01.HandlerHandleMessage.IndexOf("File Content:")
    $FileContentSegments = $http01.HandlerHandleMessage.Substring($FileContentStrIndex + 15).Split(".")
    $FileContentSegments[1] = $FileContentSegments[1].Substring(0, $FileContentSegments[1].IndexOf("]"))
    $filePath = $tmpPath + $FileContentSegments[0]
    $fileContent = $FileContentSegments[0] + "." + $FileContentSegments[1]
    Set-Content -Value $fileContent -Path $filePath

    $blobName = ".well-known\acme-challenge\" + $FileContentSegments[0]
    $blobs += $blobName
    Set-AzStorageBlobContent -File $filePath -Container "public" -Context $ctx -Blob $blobName

    Submit-ACMEChallenge -IdentifierRef $AliasDns -ChallengeType http-01
    Update-ACMEIdentifier -IdentifierRef $AliasDns
}

### UPDATE THE CERTIFICATE ###

$certAlias = 'cert-' + $aliases[0]

# Generate a new certificate for either a single domain or a domain plus SANs
if ($aliases.count -eq 1)
{
    New-ACMECertificate -Generate -IdentifierRef $aliases[0] -Alias $certAlias
}
elseif ($aliases.count -gt 1)
{
    $last = $aliases.count - 1
    New-ACMECertificate -Generate -IdentifierRef $aliases[0] -AlternativeIdentifierRefs $aliases[1 .. $last] -Alias $certAlias
}
else
{
    throw "Error: aliases has fewer than 1 alias. Either domains parameter is empty or there is a code problem"
}

# Submit the certificate request
Submit-ACMECertificate $certAlias
 
# Wait until the certificate is available (has a serial number) before moving on
# as API work in async mode so the cert may not be immediately released.
 
$serialnumber = $null
$serialnumber = $(Update-ACMECertificate -CertificateRef $certAlias).SerialNumber
 
# Export the new Certificate to a PFX file
$pfxfile = $tmpPath + "certificate.pfx"
Get-ACMECertificate -CertificateRef $certAlias -ExportPkcs12 $pfxfile -CertificatePassword $certificatePassword

# Delete blobs
foreach ($blobName in $blobs)
{
    Remove-AzStorageBlob -Container "public" -Context $ctx -Blob $blobName
}

# Update KeyVault (Application Gateway polls KV every 24 hours)
Import-AzKeyVaultCertificate -VaultName $keyVault -Name $certAlias -FilePath $pfxfile -Password $securePassword

# Delete the certificate locally
Remove-Item -Path $pfxfile -Force